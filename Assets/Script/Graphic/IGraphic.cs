using UnityEngine;
using System.Collections;

public interface IGraphic {

	Vector2 SmallestValue { get; }
	Vector2 BiggestValue { get; }
	void AddCoordinate(Vector2 val);
	void AddCoordinate(float ordinate, float abscissae);
	void UpdateGraphic();
	void ClipView(float min, float max);
	void FullView();
}

