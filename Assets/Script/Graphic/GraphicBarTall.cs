﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GraphicBarTall : GraphicBase {

	public Color barColor = Color.white;
	public bool colorTransition = false;
	public Color harderColor = Color.white;

	public float spaceBetweenBars = 1;

	public float width = 5;


	protected override void UpdateGraphic(RectTransform barInstance, Vector2 val, Vector2 normalization) {
		barInstance.anchoredPosition = new Vector2(margin + (val.x - SmallestValue.x) / normalization.x, 0);
		barInstance.sizeDelta = new Vector2(width, margin + (val.y - SmallestValue.y) / normalization.y);

		barInstance.GetComponentInChildren<Image>().color = barColor;
		if (colorTransition) {
			float valNormal = (val.y - SmallestValue.y)/(BiggestValue.y - SmallestValue.y);
			barInstance.GetComponentInChildren<Image>().color = Color.Lerp(barColor, harderColor, valNormal);
		}
	}
}
