﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GraphicContinuosTrace : GraphicBase {

	public Color traceColor = Color.white;

	protected override bool StartPlot(){
		return numbers.Count != 0;
	}

	protected override void UpdateGraphic(List<Vector2> numbers, Vector2 normalization) {
		for (int i = 0 ; i < numbers.Count-1 ; ++ i) {
			Vector2 from = numbers[i];
			Vector2 to = numbers[i+1];

			if (isClipped && (from.x < viewBoundaries.x || from.x > viewBoundaries.y)) continue;

			from = new Vector2(margin + (from.x - SmallestValue.x) / normalization.x, margin + (from.y - SmallestValue.y) / normalization.y);
			to = new Vector2(margin + (to.x - SmallestValue.x) / normalization.x, margin + (to.y - SmallestValue.y) / normalization.y);

			RectTransform traceInstance = GetNextRepresentation();
			traceInstance.GetComponentInChildren<UnityEngine.UI.Image>().color = traceColor;
			traceInstance.transform.SetParent(plotter, false);
			traceInstance.anchoredPosition = from;
			traceInstance.sizeDelta = new Vector2(1, Vector2.Distance(from, to));

			float angle = Mathf.Atan2(to.x - from.x, to.y - from.y);
			traceInstance.rotation = Quaternion.Euler(0,0, - (Mathf.Rad2Deg * angle));
		}
	}
}
