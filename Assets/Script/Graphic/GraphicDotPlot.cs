﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GraphicDotPlot : GraphicBase {

	public Color barColor = Color.white;
	public bool colorTransition = false;
	public Color harderColor = Color.white;

	protected override void UpdateGraphic(RectTransform dotInstance, Vector2 val, Vector2 normalization) {
		dotInstance.anchoredPosition = new Vector2(margin + (val.x - SmallestValue.x) / normalization.x, 
			margin + (val.y - SmallestValue.y) / normalization.y);

		dotInstance.GetComponentInChildren<Image>().color = barColor;
		if (colorTransition) {
			float valNormal = (val.y - SmallestValue.y)/(BiggestValue.y - SmallestValue.y);
			dotInstance.GetComponentInChildren<Image>().color = Color.Lerp(barColor, harderColor, valNormal);
		}
	}

}
